import pytest
import json

def test_post_dcm(test_client) :
    data = { "image": open("data/1_1.dcm", "rb"), "model": "cheXnet", "modelVersion" : "ab0f1fe1f2ff0f4f2544b93b8b58e4b1" }
    
    response = test_client.post("/bounding-box" , data=data)
    
    data = response.json
    print(json.dumps(data, indent = 3))
    
    assert response.status_code == 200
    # assert data['status'] == 'Success'

def test_post_jpeg(test_client) :
    data = { "image": open("data/1_1.jpeg", "rb"), "model": "cheXnet", "modelVersion" : "ab0f1fe1f2ff0f4f2544b93b8b58e4b1" }
    
    response = test_client.post("/bounding-box" , data=data)
    
    data = response.json
    print(json.dumps(data, indent = 3))
    
    assert response.status_code == 200
    # assert data['status'] == 'Success'
    
def test_get(test_client) :
    response = test_client.get("/bounding-box")
    
    data = response.json
    print(json.dumps(data, indent = 3))
    
    assert response.status_code == 200
    assert data['status'] == 'Success'